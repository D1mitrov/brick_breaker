import 'package:casse_brick/widgets/game_app.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const GameApp());
}